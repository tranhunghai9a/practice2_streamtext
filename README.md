# pratice2_stream

- Giả lập một Stream instance gởi liên tục các tự vựng tiếng Anh (text).
- Viết giao diện để nhận text từ Stream và xử lý cho các nhu cầu sau:
  1) chuyển đổi tất cả text nhận thành chữ thường, và log ra Backend.
  2) Đến số text đã nhận được và hiển thị realtime lên màn hình Flutter App.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
