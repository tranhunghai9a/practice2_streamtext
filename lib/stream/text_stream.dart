import 'package:flutter/material.dart';

class TextStream {
  // Stream textStream;

  Stream<String> getTexts() async* {
    final List<String> texts = [
      "LOVE",
      "HATE",
      "KILL",
      "SAD"
    ];

    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      int index = t % 5;
      return texts[index].toLowerCase();
    });
  }
}