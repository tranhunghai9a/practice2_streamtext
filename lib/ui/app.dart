import 'package:flutter/material.dart';
import '../stream/text_stream.dart';

// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return HomePageStream();
//   }
//
// }

class App extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class HomePageState extends State<StatefulWidget> {
  String text ='press the button and the text here will change';
  TextStream textStream = TextStream();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Text',

      home: Scaffold(
        appBar: AppBar(title: Text('Change Text App')),
        body: Text(text.toLowerCase()),
        floatingActionButton: FloatingActionButton(
          child: Text('C'),
          onPressed: () {
            changeText();
          },
        ),
      ),
    );
  }

  changeText() async {
    textStream.getTexts().listen((eventText) {
      setState(() {
        print(eventText);
        text = eventText;
      });
    });
  }
}